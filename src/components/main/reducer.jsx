/**
 * Our main reducer
 */
import * as constants from './constants';
import data from '../../../data/dishes.json';

/**
 * Set the initial state for this widget
 *
 * @return {object} - The initial state
 */
const initial = () => ({
    data,
    choices: [
        {
            index: 0,
            isValid: false,
            numServings: '1',
            selectedDish: '',
        },
    ],
    maxServings: 10,
    totalServings: 0,
});

/**
 * Remove duplicates by given property name from an array of objects
 *
 * @param {array} givenArray - The array
 * @param {string} propToCheck - The property to check against
 * @returns {array}
 */
const removeDuplicates = (givenArray, propToCheck) =>
    givenArray.filter(
        (obj, pos, arr) => arr.map(mapObj => mapObj[propToCheck]).indexOf(obj[propToCheck]) === pos
    );

/**
 * Constrain a input of type 'number' to a max value, even on manual input
 *
 * @param {number} original - The original number before going over the max
 * @param {number} num - The given number
 * @returns {string}
 */
const constrainNumberInput = (original, num) => {
    if (num > initial().maxServings) {
        return `${original}`;
    }

    return num === '' || num === '0' ? '1' : num;
};

const choice = (state, action, index) => {
    switch (action.type) {
        case constants.CHANGE_DISH:
            return {
                ...state,
                isValid:
                    action.index === index
                        ? state.numServings !== '' && action.dish !== ''
                        : state.isValid,
                selectedDish: action.index === index ? action.dish : state.selectedDish,
            };

        case constants.CHANGE_SERVING:
            return {
                ...state,
                isValid:
                    action.index === index
                        ? action.numServings !== '' && state.selectedDish !== ''
                        : state.isValid,
                numServings:
                    action.index === index
                        ? constrainNumberInput(state.numServings, action.serving)
                        : state.numServings,
            };

        default:
            return state;
    }
};

const dish = (state, action) => {
    switch (action.type) {
        case constants.CHANGE_DISH:
            return {
                ...state,
                selected: state.name === action.dish ? true : state.selected || false,
            };

        case constants.REMOVE_LAST_CHOICE:
            return {
                ...state,
                selected: state.name === action.selection.selectedDish ? false : state.selected,
            };

        case constants.UPDATE_SELECTED_DISHES:
            return {
                ...state,
                selected: state.name === action.original ? false : state.selected,
            };

        default:
            return state;
    }
};

/**
 * Change the state of this widget
 *
 * @param {object} state - The redux state
 * @param {object} action - The action dispatcher as defined in actions.jsx
 * @return {object} - A copy of the state
 */
const main = (state = initial(), action) => {
    switch (action.type) {
        case constants.ADD_NEW_CHOICE:
            return {
                ...state,
                choices: [
                    ...state.choices,
                    {
                        index: state.choices.length,
                        isValid: false,
                        numServings: '1',
                        selectedDish: '',
                    },
                ],
            };

        case constants.CHANGE_DISH:
        case constants.CHANGE_SERVING:
            return {
                ...state,
                choices: state.choices.map((c, index) => choice(c, action, index)),
                dishes: state.dishes.map(d => dish(d, action)),
            };

        case constants.CHANGE_MEAL:
            return {
                ...state,
                availableDishes: data.dishes.filter(d => d.availableMeals.includes(action.value)),
                restaurants: removeDuplicates(
                    data.dishes.filter(d => d.availableMeals.includes(action.value)),
                    'restaurant'
                ),
                selectedMeal: action.value,
                selectedRestaurant: '',
            };

        case constants.CHANGE_PEOPLE:
            return {
                ...state,
                selectedNumPeople: constrainNumberInput(state.selectedNumPeople || 1, action.value),
            };

        case constants.CHANGE_RESTAURANT:
            return {
                ...state,
                choices: initial().choices,
                dishes: removeDuplicates(
                    state.availableDishes.filter(d => d.restaurant === action.value),
                    'name'
                ),
                selectedRestaurant: action.value,
                totalServings: 0,
            };

        case constants.REMOVE_LAST_CHOICE:
            return {
                ...state,
                choices: [...state.choices.slice(0, state.choices.length - 1)],
                dishes: state.dishes.map(d => dish(d, action)),
            };

        case constants.UPDATE_SELECTED_DISHES:
            return {
                ...state,
                dishes: state.dishes.map(d => dish(d, action)),
            };

        case constants.UPDATE_TOTAL_SERVINGS:
            return {
                ...state,
                totalServings: state.choices.reduce(
                    (acc, value) =>
                        constrainNumberInput(+value.numServings, +value.numServings) + acc,
                    0
                ),
            };

        default:
            return state;
    }
};

export default main;
