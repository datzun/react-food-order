import * as constants from './constants';

/**
 * @returns {object}
 */
export const addNewChoiceEntry = () => dispatch =>
    Promise.resolve()
        .then(() =>
            dispatch({
                type: constants.ADD_NEW_CHOICE,
            })
        )
        .then(() => dispatch(updateTotalServings()));

/**
 * @param {string} dish
 * @param {number} index
 * @param {string} original
 * @return {object}
 */
export const handleDishChange = (dish, index, original) => dispatch =>
    Promise.resolve()
        .then(() =>
            dispatch({
                type: constants.CHANGE_DISH,
                dish,
                index,
            })
        )
        .then(() =>
            dispatch({
                type: constants.UPDATE_SELECTED_DISHES,
                original,
            })
        )
        .then(() => dispatch(updateTotalServings()));

/**
 * @param {string} value
 * @return {object}
 */
export const handleMealChange = value => ({
    type: constants.CHANGE_MEAL,
    value,
});

/**
 * @param {string} value
 * @return {object}
 */
export const handlePeopleChange = value => ({
    type: constants.CHANGE_PEOPLE,
    value,
});

/**
 * @param {string} value
 * @return {object}
 */
export const handleRestaurantChange = value => ({
    type: constants.CHANGE_RESTAURANT,
    value,
});

/**
 * @param {string} serving
 * @param {number} index
 * @return {object}
 */
export const handleServingChange = (serving, index) => dispatch =>
    Promise.resolve()
        .then(() =>
            dispatch({
                type: constants.CHANGE_SERVING,
                serving,
                index,
            })
        )
        .then(() => dispatch(updateTotalServings()));

/**
 * @param {object} selection
 * @returns {object}
 */
export const removeLastChoice = selection => dispatch =>
    Promise.resolve()
        .then(() =>
            dispatch({
                type: constants.REMOVE_LAST_CHOICE,
                selection,
            })
        )
        .then(() => dispatch(updateTotalServings()));

/**
 * @param {object} order
 * @returns {object}
 */
export const sendOrder = order => dispatch => {
    console.log(order);
    return dispatch({
        type: null
    });
};

/**
 * @returns {object}
 */
const updateTotalServings = () => ({
    type: constants.UPDATE_TOTAL_SERVINGS,
});
