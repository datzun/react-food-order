import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

let wrapper;

beforeEach(() => {
	wrapper = shallow(<Module
		choices={[{
			isValid: false,
			numServings: '2',
			selectedDish: 'dish'
		},{
			isValid: false,
			numServings: '2',
			selectedDish: 'dish2'
		}]}
		goBack={() => {}}
		handlePanelChange={() => {}}
		selectedMeal="breakfast"
		selectedNumPeople="2"
		selectedRestaurant="Olive Garden"
        sendOrder={() => {}}
	/>);
});

describe('PanelFour Component (Snapshot)', () => {
	it('renders PanelFour component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});