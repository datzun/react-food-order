/**
 * @exports PanelFour
 */

import PropTypes from 'prop-types';
import React from 'react';
import Button from '../../button/';
import './styles.scss';

/**
 * The panel display
 *
 * @param {object} props - Passed properties
 * * @param {array} props.choices - The line item choices
 * @param {function} props.goBack - Handles navigating back
 * @param {function} props.handlePanelChange - Handles navigating to new panel
 * @param {number} props.maxServings - The max allowed servings
 * @param {string} props.selectedMeal - The selected meal (e.g. breakfast, lunch, or dinner)
 * @param {string} props.selectedNumPeople - The number of people to serve
 * @param {string} props.selectedRestaurant - The selected restaurant (e.g. McDonalds, Olive Garden, etc)
 * @param {function} props.sendOrder - Sends the order
 * @constructor
 * @returns {XML} - PanelFour
 */
const PanelFour = props => (
    <div className="panel review">
        <React.Fragment>
            <div>
                <span>Meal:</span>
                <span>{props.selectedMeal}</span>
            </div>
            <div>
                <span># of People:</span>
                <span>{props.selectedNumPeople}</span>
            </div>
            <div>
                <span>Restaurant:</span>
                <span>{props.selectedRestaurant}</span>
            </div>
            <div>
                <span>Dishes:</span>
            </div>
            <div>
                {props.choices.map(choice => (
                    <React.Fragment key={choice.selectedDish}>
                        <span>
                            {choice.selectedDish} - {choice.numServings}
                        </span>
                    </React.Fragment>
                ))}
            </div>
        </React.Fragment>
        <footer className="button-container">
            <Button id="previous-button" onClick={() => props.goBack()} text="Previous" />
            <Button
                id="done-button"
                onClick={() => {
                    props.sendOrder({
                        choices: props.choices,
                        meal: props.selectedMeal,
                        people: props.selectedNumPeople,
                        restaurant: props.selectedRestaurant
                    });
                    props.handlePanelChange('done');
                }}
                text="Order"
            />
        </footer>
    </div>
);

PanelFour.propTypes = {
    choices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    goBack: PropTypes.func.isRequired,
    handlePanelChange: PropTypes.func.isRequired,
    selectedMeal: PropTypes.string.isRequired,
    selectedNumPeople: PropTypes.string.isRequired,
    selectedRestaurant: PropTypes.string.isRequired,
    sendOrder: PropTypes.func.isRequired,
};

export default PanelFour;
