import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

let wrapper;

beforeEach(() => {
	wrapper = shallow(<Module
		addNewChoiceEntry={() => {}}
		choices={[{
		    index: 0,
			isValid: false,
			numServings: '2',
			selectedDish: 'dish'
		},{
		    index: 1,
			isValid: false,
			numServings: '2',
			selectedDish: 'dish2'
		}]}
		dishes={[
			{ name: 'dish', selected: true },
			{ name: 'dish2', selected: false }
		]}
		goBack={() => {}}
		handleDishChange={() => {}}
		handlePanelChange={() => {}}
		handleServingChange={() => {}}
		maxServings={10}
		removeLastChoice={() => {}}
		selectedNumPeople="1"
	/>);
});

describe('PanelThree Component (Snapshot)', () => {
	it('renders PanelThree component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});