/**
 * @exports PanelThreeContainer
 */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { goBack, push } from 'react-router-redux';
import Component from './';
import * as Actions from '../actions';

/**
 * Pass data to the component
 *
 * @param {object} state - The redux object
 * @param {object} ownProps - The passed parameters to the container
 * @returns {object} - Component data
 */
const mapStateToProps = (state, ownProps) => ({
    ...state.mainReducer,
    ...ownProps,
});

/**
 * Event handlers for the component
 *
 * @param {function} dispatch - The redux dispatcher
 * @returns {object} - Component functions
 */
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            /**
             * Adds a new choice entry for dish / serving
             *
             * @return {object}
             */
            addNewChoiceEntry: () => Actions.addNewChoiceEntry(),
            /**
             * Handle going back a panel
             *
             * @return {object}
             */
            goBack: () => goBack(),
            /**
             * Handle selection of dish
             *
             * @param {string} value - The dish value
             * @param {string|number} index - The entry to update
             * @param {string} original - The current dish selected
             * @return {object}
             */
            handleDishChange: (value, index, original) =>
                Actions.handleDishChange(value, index, original),
            /**
             * Handle changing to the next panel
             *
             * @param {string} panel - The panel to navigate to
             * @return {object}
             */
            handlePanelChange: panel => push(`/${panel}`),
            /**
             * Handle selection of servings
             *
             * @param {string} value - The serving value
             * @param {string|number} index - The entry to update
             * @return {object}
             */
            handleServingChange: (value, index) => Actions.handleServingChange(value, index),
            /**
             * Remove the last choice
             *
             * @param {object} selection - The item being deleted
             * @return {object}
             */
            removeLastChoice: selection => Actions.removeLastChoice(selection),
        },
        dispatch
    );

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);

export default Container;
