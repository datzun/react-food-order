/**
 * @exports PanelThree
 */

import PropTypes from 'prop-types';
import React from 'react';
import ReactTooltip from 'react-tooltip';
import Button from '../../button/';
import Input from '../../input/';
import Select from '../../select/';
import './styles.scss';

/**
 * Dish selection
 *
 * @param {object} props - Passed properties
 * @param {function} props.addNewChoiceEntry - Add a new line item
 * @param {array} props.choices - The line item choices
 * @param {array} props.dishes - The available dishes from this restaurant / for this meal
 * @param {function} props.handleDishChange - Handles changing the dish for the current line item
 * @param {function} props.handleServingChange - Handles changing the serving amount for the current line item
 * @param {number} props.maxServings - The max allowed servings
 * @param {function} props.removeLastChoice - Remove a line item
 * @param {number} props.totalServings - The total number of servings selected
 * @constructor
 * @returns {XML} - dishSelection
 */
const dishSelection = props => (
    <React.Fragment>
        {props.choices.map((selection, index) => (
            <div className="choice" key={selection.index}>
                <Select
                    id={`dish-${index}`}
                    onChange={e =>
                        props.handleDishChange(e.target.value, index, selection.selectedDish)
                    }
                    options={[
                        {
                            disabled: true,
                            label: 'Select...',
                            value: '',
                        },
                        ...props.dishes.map(dish => ({
                            disabled: dish.selected,
                            label: dish.name,
                            value: dish.name,
                        })),
                    ]}
                    value={selection.selectedDish}
                />
                <Input
                    id={`servings-${index}`}
                    max={props.maxServings}
                    min={1}
                    onChange={e => props.handleServingChange(e.target.value, index)}
                    type="number"
                    value={selection.numServings}
                />
                {selection.selectedDish !== '' &&
                    props.totalServings < props.maxServings &&
                    props.choices.length - 1 === index &&
                    !props.dishes.every(d => d.selected === true) && (
                        <Button
                            disabled={selection.selectedDish === '' || selection.numServings === ''}
                            id="serving-button"
                            onClick={() => props.addNewChoiceEntry()}
                            text="+"
                        />
                    )}
                {props.choices.length !== 1 &&
                    props.choices.length - 1 === index && (
                        <Button
                            id="serving-button"
                            onClick={() => props.removeLastChoice(selection)}
                            text="-"
                        />
                    )}
            </div>
        ))}
    </React.Fragment>
);

dishSelection.propTypes = {
    choices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    dishes: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    maxServings: PropTypes.number.isRequired,
    totalServings: PropTypes.number.isRequired,
};

/**
 * The panel display
 *
 * @param {object} props - Passed properties
 * @param {function} props.goBack - Handles navigating back
 * @param {function} props.handlePanelChange - Handles navigating to new panel
 * @param {number} props.maxServings - The max allowed servings
 * @param {string} props.selectedNumPeople - The number of people to serve
 * @param {number} props.totalServings - The total number of servings selected
 * @constructor
 * @returns {XML} - PanelThree
 */
const PanelThree = props => (
    <div className="panel">
        <div className="headings">
            <span>Please Select Dish(es)</span>
            <span># of Serving(s)</span>
        </div>
        {dishSelection(props)}
        <footer className="button-container">
            <Button id="previous-button" onClick={() => props.goBack()} text="Previous" />
            <div data-tip data-for="serving-error-tip">
                <Button
                    disabled={
                        props.maxServings < props.totalServings ||
                        props.totalServings < props.selectedNumPeople ||
                        !props.choices.every(c => c.isValid === true)
                    }
                    id="next-button"
                    onClick={() => props.handlePanelChange('review')}
                    text="Review"
                />
            </div>
            {
                (props.maxServings < props.totalServings ||
                    props.totalServings < props.selectedNumPeople ||
                    !props.choices.every(c => c.isValid === true)) &&
                    <ReactTooltip id="serving-error-tip" place="bottom" type="error">
                        <ul>
                            {
                                props.maxServings < props.totalServings && <li>
                                    <span>You are allowed 10 servings maximum.</span>
                                </li>
                            }
                            {
                                props.totalServings < props.selectedNumPeople && <li>
                                    <span>Total servings must be greater than
                                        or equal to the number of people.</span>
                                </li>
                            }
                            {
                                !props.choices.every(c => c.isValid === true) && <li>
                                    <span>Please ensure that all items are filled in.</span>
                                </li>
                            }
                        </ul>
                    </ReactTooltip>
            }
        </footer>
    </div>
);

PanelThree.defaultProps = {
    totalServings: 0,
};

PanelThree.propTypes = {
    choices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    goBack: PropTypes.func.isRequired,
    handlePanelChange: PropTypes.func.isRequired,
    maxServings: PropTypes.number.isRequired,
    selectedNumPeople: PropTypes.string.isRequired,
    totalServings: PropTypes.number,
};

export default PanelThree;
