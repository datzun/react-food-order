/**
 * Initial rendering
 */
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Store from '../../utils/store';
import MainRouter from './router';
import './styles.scss';

render(
    <Provider store={Store}>
        <MainRouter />
    </Provider>,
    document.getElementById('root')
);
