/**
 * @exports PanelTwo
 */

import PropTypes from 'prop-types';
import React from 'react';
import ReactTooltip from 'react-tooltip';
import Button from '../../button/';
import Select from '../../select/';

/**
 * The panel display
 *
 * @param {object} props - Passed properties
 * @param {function} props.goBack - Handles navigating back
 * @param {function} props.handlePanelChange - Handles navigating to new panel
 * @param {function} props.handleRestaurantChange - Handles changing the restaurant
 * @param {array} props.restaurants - The available restaurants for this meal
 * @param {string} props.selectedRestaurant - The selected restaurant (e.g. McDonalds, Olive Garden, etc)
 * @constructor
 * @returns {XML} - PanelTwo
 */
const PanelTwo = props => (
    <div className="panel">
        <Select
            id="restaurants"
            label="Please Select a Restaurant"
            onChange={e => props.handleRestaurantChange(e.target.value)}
            options={[
                {
                    disabled: true,
                    label: 'Select...',
                    value: '',
                },
                ...props.restaurants.map(restaurant => ({
                    disabled: false,
                    label: restaurant.restaurant,
                    value: restaurant.restaurant,
                })),
            ]}
            value={props.selectedRestaurant || ''}
        />
        <footer className="button-container">
            <Button id="previous-button" onClick={() => props.goBack()} text="Previous" />
            <div data-tip data-for="restaurant-error-tip">
                <Button
                    disabled={props.selectedRestaurant === ''}
                    id="restaurant-button"
                    onClick={() => props.handlePanelChange('dishes')}
                    text="Next"
                />
            </div>
            {
                (props.selectedRestaurant === '') &&
                <ReactTooltip id="restaurant-error-tip" place="bottom" type="error">
                    <ul>
                        {
                            props.selectedRestaurant === '' && <li>
                                <span>You must select a restaurant.</span>
                            </li>
                        }
                    </ul>
                </ReactTooltip>
            }
        </footer>
    </div>
);

PanelTwo.defaultProps = {
    selectedRestaurant: '',
};

PanelTwo.propTypes = {
    goBack: PropTypes.func.isRequired,
    handlePanelChange: PropTypes.func.isRequired,
    handleRestaurantChange: PropTypes.func.isRequired,
    restaurants: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
    selectedRestaurant: PropTypes.string,
};

export default PanelTwo;
