import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

let wrapper;

beforeEach(() => {
	wrapper = shallow(<Module
		goBack={() => {}}
		handlePanelChange={() => {}}
		handleRestaurantChange={() => {}}
		restaurants={[{
			restaurant: 'Olive Garden'
		}]}
	/>);
});

describe('PanelTwo Component (Snapshot)', () => {
	it('renders PanelTwo component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});