/**
 * Router Setup
 */
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { history } from '../../utils/store';
import Breadcrumbs from '../breadcrumbs/';
import Done from '../done/';
import PanelOneContainer from './panelOne/container';
import PanelTwoContainer from './panelTwo/container';
import PanelThreeContainer from './panelThree/container';
import PanelFourContainer from './panelFour/container';

/**
 * Our routing of the app which brings in the main components / panels
 *
 * @returns {*} MainRouter
 * @constructor
 */
const MainRouter = () => (
    <ConnectedRouter history={history}>
        <div className="main-container">
            <Breadcrumbs
                crumbs={[
                    {
                        name: 'Step 1',
                        path: '/',
                    },
                    {
                        name: 'Step 2',
                        path: '/restaurant',
                    },
                    {
                        name: 'Step 3',
                        path: '/dishes',
                    },
                    {
                        name: 'Review',
                        path: '/review',
                    },
                ]}
            />
            <Switch>
                <Route exact path="/" component={PanelOneContainer} />
                <Route path="/restaurant" component={PanelTwoContainer} />
                <Route path="/dishes" component={PanelThreeContainer} />
                <Route path="/review" component={PanelFourContainer} />
                <Route path="/done" component={Done} />
                <Route component={PanelOneContainer} />
            </Switch>
        </div>
    </ConnectedRouter>
);

export default MainRouter;
