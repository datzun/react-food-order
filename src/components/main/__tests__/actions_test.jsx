import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from './../actions';
import * as constants from './../constants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Actions - ', () => {
	it('addNewChoiceEntry', () => {
		const expectedActions = [{
			type: constants.ADD_NEW_CHOICE
		},{
			type: constants.UPDATE_TOTAL_SERVINGS
		}];

		const store = mockStore({});

		return store.dispatch(actions.addNewChoiceEntry()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});

	it('handleDishChange', () => {
		const expectedActions = [{
			type: constants.CHANGE_DISH,
			dish: 'dish',
			index: 0
		},{
			type: constants.UPDATE_SELECTED_DISHES,
			original: 'original'
		},{
			type: constants.UPDATE_TOTAL_SERVINGS
		}];

		const store = mockStore({});

		return store.dispatch(actions.handleDishChange('dish', 0, 'original')).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});

	it('handleMealChange', () => {
		const expectedAction = {
			type: constants.CHANGE_MEAL,
			value: 'value'
		};

		expect(actions.handleMealChange('value')).toEqual(expectedAction);
	});

	it('handlePeopleChange', () => {
		const expectedAction = {
			type: constants.CHANGE_PEOPLE,
			value: 'value'
		};

		expect(actions.handlePeopleChange('value')).toEqual(expectedAction);
	});

	it('handleRestaurantChange', () => {
		const expectedAction = {
			type: constants.CHANGE_RESTAURANT,
			value: 'value'
		};

		expect(actions.handleRestaurantChange('value')).toEqual(expectedAction);
	});

	it('handleServingChange', () => {
		const expectedActions = [{
			type: constants.CHANGE_SERVING,
			serving: '1',
			index: 0
		}, {
			type: constants.UPDATE_TOTAL_SERVINGS
		}];

		const store = mockStore({});

		return store.dispatch(actions.handleServingChange('1', 0)).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});

	it('removeLastChoice', () => {
		const expectedActions = [{
			type: constants.REMOVE_LAST_CHOICE,
			selection: {}
		},{
			type: constants.UPDATE_TOTAL_SERVINGS
		}];

		const store = mockStore({});

		return store.dispatch(actions.removeLastChoice({})).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
});