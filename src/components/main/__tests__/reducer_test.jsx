import * as constants from '../constants';
import reducer from './../reducer';
import data from '../../../../data/dishes.json';

const initialState = {
	data,
	choices: [{
	    index: 0,
		isValid: false,
		numServings: '1',
		selectedDish: ''
	}],
	maxServings: 10,
	totalServings: 0
};

describe('main reducer', () => {
	it('should return the initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState);
	});

	it('should handle ADD_NEW_CHOICE', () => {
		expect(
			reducer(initialState, {
				type: constants.ADD_NEW_CHOICE,
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}, {
			    index: 1,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			maxServings: 10,
			totalServings: 0
		});
	});

	it('should handle CHANGE_DISH', () => {
		expect(
			reducer({
				...initialState,
				dishes: [{ name: 'dish', selected: false }]
			}, {
				type: constants.CHANGE_DISH,
				dish: 'dish',
				index: 0
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: true,
				numServings: '1',
				selectedDish: 'dish'
			}],
			dishes: [{ name: 'dish', selected: true }],
			maxServings: 10,
			totalServings: 0
		});
	});

	it('should handle CHANGE_SERVING', () => {
		expect(
			reducer({
				...initialState,
				dishes: []
			}, {
				type: constants.CHANGE_SERVING,
				serving: '1',
				index: 0
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			dishes: [],
			maxServings: 10,
			totalServings: 0
		});
	});

	it('should handle CHANGE_MEAL', () => {
		expect(
			reducer(initialState, {
				type: constants.CHANGE_MEAL,
				value: 'breakfast'
			})
		).toEqual({
			availableDishes: [
				{
					"id": 5,
					"name": "Egg Muffin",
					"restaurant": "Mc Donalds",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 14,
					"name": "Coleslaw Sandwich",
					"restaurant": "Vege Deli",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 15,
					"name": "Grilled Sandwich",
					"restaurant": "Vege Deli",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 30,
					"name": "Garlic Bread",
					"restaurant": "Olive Garden",
					"availableMeals": [
						"breakfast",
						"lunch",
						"dinner"
					]
				}
			],
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			restaurants: [
				{
					"id": 5,
					"name": "Egg Muffin",
					"restaurant": "Mc Donalds",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 14,
					"name": "Coleslaw Sandwich",
					"restaurant": "Vege Deli",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 30,
					"name": "Garlic Bread",
					"restaurant": "Olive Garden",
					"availableMeals": [
						"breakfast",
						"lunch",
						"dinner"
					]
				}
			],
			maxServings: 10,
			selectedMeal: 'breakfast',
			selectedRestaurant: '',
			totalServings: 0
		});
	});

	it('should handle CHANGE_PEOPLE', () => {
		expect(
			reducer(initialState, {
				type: constants.CHANGE_PEOPLE,
				value: '5'
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			maxServings: 10,
			selectedNumPeople: '5',
			totalServings: 0
		});
	});

	it('should handle CHANGE_RESTAURANT', () => {
		expect(
			reducer({
				...initialState,
				availableDishes: [
					{
						"id": 5,
						"name": "Egg Muffin",
						"restaurant": "Mc Donalds",
						"availableMeals": [
							"breakfast"
						]
					},
					{
						"id": 14,
						"name": "Coleslaw Sandwich",
						"restaurant": "Vege Deli",
						"availableMeals": [
							"breakfast"
						]
					},
					{
						"id": 15,
						"name": "Grilled Sandwich",
						"restaurant": "Vege Deli",
						"availableMeals": [
							"breakfast"
						]
					},
					{
						"id": 30,
						"name": "Garlic Bread",
						"restaurant": "Olive Garden",
						"availableMeals": [
							"breakfast",
							"lunch",
							"dinner"
						]
					}
				]
			}, {
				type: constants.CHANGE_RESTAURANT,
				value: 'Olive Garden'
			})
		).toEqual({
			availableDishes: [
				{
					"id": 5,
					"name": "Egg Muffin",
					"restaurant": "Mc Donalds",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 14,
					"name": "Coleslaw Sandwich",
					"restaurant": "Vege Deli",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 15,
					"name": "Grilled Sandwich",
					"restaurant": "Vege Deli",
					"availableMeals": [
						"breakfast"
					]
				},
				{
					"id": 30,
					"name": "Garlic Bread",
					"restaurant": "Olive Garden",
					"availableMeals": [
						"breakfast",
						"lunch",
						"dinner"
					]
				}
			],
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			dishes: [
				{
					"id": 30,
					"name": "Garlic Bread",
					"restaurant": "Olive Garden",
					"availableMeals": [
						"breakfast",
						"lunch",
						"dinner"
					]
				}
			],
			maxServings: 10,
			selectedRestaurant: 'Olive Garden',
			totalServings: 0
		});
	});

	it('should handle REMOVE_LAST_CHOICE', () => {
		expect(
			reducer({
				...initialState,
				dishes: [{ name: 'dish', selected: true }]
			}, {
				type: constants.REMOVE_LAST_CHOICE,
				selection: {
					selectedDish: 'dish'
				}
			})
		).toEqual({
			data,
			choices: [],
			dishes: [{ name: 'dish', selected: false }],
			maxServings: 10,
			totalServings: 0
		});
	});

	it('should handle UPDATE_SELECTED_DISHES', () => {
		expect(
			reducer({
				...initialState,
				dishes: [
					{ name: 'dish', selected: true },
					{ name: 'dish2', selected: true }
				]
			}, {
				type: constants.UPDATE_SELECTED_DISHES,
				original: 'dish'
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '1',
				selectedDish: ''
			}],
			dishes: [
				{ name: 'dish', selected: false },
				{ name: 'dish2', selected: true}
			],
			maxServings: 10,
			totalServings: 0
		});
	});

	it('should handle UPDATE_TOTAL_SERVINGS', () => {
		expect(
			reducer({
				data,
				choices: [{
				    index: 0,
					isValid: false,
					numServings: '2',
					selectedDish: ''
				},{
				    index: 1,
					isValid: false,
					numServings: '2',
					selectedDish: ''
				}],
				maxServings: 10,
				totalServings: 0
			}, {
				type: constants.UPDATE_TOTAL_SERVINGS
			})
		).toEqual({
			data,
			choices: [{
			    index: 0,
				isValid: false,
				numServings: '2',
				selectedDish: ''
			},{
			    index: 1,
				isValid: false,
				numServings: '2',
				selectedDish: ''
			}],
			maxServings: 10,
			totalServings: 4
		});
	});
});
