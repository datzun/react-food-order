/**
 * Integration tests
 *
 * @todo: Work in progress, still need to figure out routing (see setupIntegrationTest)
 */
import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import setupIntegrationTest from '../../../utils/testStore';
import Module from '../router';

let store;
let dispatchSpy;
let wrapper;

beforeEach(() => {
    ({ store, dispatchSpy } = setupIntegrationTest());
});

describe('Main Integration', () => {
    it('renders Main component', () => {
        wrapper = mount(<Provider store={store}><Module /></Provider>);
        expect(wrapper.find('.main-container').exists()).toBe(true);
        expect(wrapper.find('#meal').exists()).toBe(true);
        expect(wrapper.find('#numPeople').exists()).toBe(true);
    });

    it('1st Panel user interactions', () => {
        wrapper = mount(<Provider store={store}><Module /></Provider>);

        const mealSelect = wrapper.find('select#meal');
        mealSelect.simulate('change', {target: {value: 'breakfast'}});
        expect(dispatchSpy).toBeCalledWith({ type: 'main/CHANGE_MEAL', value: 'breakfast' });
        expect(store.getState().mainReducer.selectedMeal).toEqual('breakfast');

        const numPeopleInput = wrapper.find('input#numPeople');
        numPeopleInput.simulate('change', {target: {value: '1'}});
        expect(dispatchSpy).toBeCalledWith({ type: 'main/CHANGE_PEOPLE', value: '1' });
        expect(store.getState().mainReducer.selectedNumPeople).toEqual('1');

        expect(wrapper.find('button#meal-button').exists()).toBe(true);
        wrapper.find('button#meal-button').simulate('click');
        expect(dispatchSpy).toBeCalledWith({
            'type': '@@router/LOCATION_CHANGE',
            'payload': {
                'pathname': '/',
                'search': '',
                'hash': ''
            }
        });
    });
});