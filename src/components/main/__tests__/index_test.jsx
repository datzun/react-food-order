import React from 'react';
import { shallow } from 'enzyme';
import Module from '../router';

describe('Main', () => {
	it('should render app', () => {
		const component = shallow(<Module />);
		expect(component).toMatchSnapshot();
	});
});
