/**
 * @exports PanelOneContainer
 */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import Component from './';
import * as Actions from '../actions';

/**
 * Pass data to the component
 *
 * @param {object} state - The redux object
 * @param {object} ownProps - The passed parameters to the container
 * @returns {object} - Component data
 */
const mapStateToProps = (state, ownProps) => ({
    ...state.mainReducer,
    ...ownProps,
});

/**
 * Event handlers for the component
 *
 * @param {function} dispatch - The redux dispatcher
 * @returns {object} - Component functions
 */
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            /**
             * Handle selection of meal option
             *
             * @param {string} value - The meal option value
             * @return {object}
             */
            handleMealChange: value => Actions.handleMealChange(value),
            /**
             * Handle changing to the next panel
             *
             * @param {string} panel - The panel to navigate to
             * @return {object}
             */
            handlePanelChange: panel => push(`/${panel}`),
            /**
             * Handle selection of number of people
             *
             * @param {string} value - The meal option value
             * @return {object}
             */
            handlePeopleChange: value => Actions.handlePeopleChange(value),
        },
        dispatch
    );

const Container = connect(mapStateToProps, mapDispatchToProps)(Component);

export default Container;
