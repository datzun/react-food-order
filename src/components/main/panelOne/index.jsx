/**
 * @exports PanelOne
 */

import PropTypes from 'prop-types';
import React from 'react';
import ReactTooltip from 'react-tooltip';
import Button from '../../button/';
import Input from '../../input/';
import Select from '../../select/';

/**
 * The panel display
 *
 * @todo: Potentially refactor these panels into a single panel controlled by configuration
 *
 * @param {object} props - Passed properties
 * @param {function} props.handleMealChange - Handles the changing of the meal
 * @param {function} props.handlePanelChange - Handles navigating to new panel
 * @param {function} props.handlePeopleChange - Handles changing the amount of people to serve
 * @param {string} props.selectedMeal - The selected meal (e.g. breakfast, lunch, or dinner)
 * @param {string} props.selectedNumPeople - The number of people to serve
 * @constructor
 * @returns {XML} - PanelOne
 */
const PanelOne = props => (
    <div className="panel">
        <Select
            id="meal"
            label="Please Select a Meal"
            onChange={e => props.handleMealChange(e.target.value)}
            options={[
                {
                    disabled: true,
                    label: 'Select...',
                    value: '',
                },
                {
                    disabled: false,
                    label: 'Breakfast',
                    value: 'breakfast',
                },
                {
                    disabled: false,
                    label: 'Lunch',
                    value: 'lunch',
                },
                {
                    disabled: false,
                    label: 'Dinner',
                    value: 'dinner',
                },
            ]}
            value={props.selectedMeal}
        />
        <Input
            id="numPeople"
            label="Please Enter Number of People"
            max={10}
            min={1}
            onChange={e => props.handlePeopleChange(e.target.value)}
            type="number"
            value={props.selectedNumPeople}
        />
        <footer className="button-container one">
            <div data-tip data-for="meal-error-tip">
                <Button
                    disabled={props.selectedMeal === '' || props.selectedNumPeople === ''}
                    id="meal-button"
                    onClick={() => props.handlePanelChange('restaurant')}
                    text="Next"
                />
            </div>
            {
                (props.selectedMeal === '' || props.selectedNumPeople === '') &&
                <ReactTooltip id="meal-error-tip" place="bottom" type="error">
                    <ul>
                        {
                            props.selectedMeal === '' && <li>
                                <span>You must select a meal.</span>
                            </li>
                        }
                        {
                            props.selectedNumPeople === '' && <li>
                                <span>You must enter the # of people (max: 10).</span>
                            </li>
                        }
                    </ul>
                </ReactTooltip>
            }
        </footer>
    </div>
);

PanelOne.defaultProps = {
    selectedMeal: '',
    selectedNumPeople: '',
};

PanelOne.propTypes = {
    handleMealChange: PropTypes.func.isRequired,
    handlePanelChange: PropTypes.func.isRequired,
    handlePeopleChange: PropTypes.func.isRequired,
    selectedMeal: PropTypes.string,
    selectedNumPeople: PropTypes.string,
};

export default PanelOne;
