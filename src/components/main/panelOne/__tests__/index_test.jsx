import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

let wrapper;

beforeEach(() => {
	wrapper = shallow(<Module
		handleMealChange={() => {}}
		handlePanelChange={() => {}}
		handlePeopleChange={() => {}}
	/>);
});

describe('PanelOne Component (Snapshot)', () => {
	it('renders PanelOne component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});