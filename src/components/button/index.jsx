/**
 * @exports Button
 */

import PropTypes from 'prop-types';
import React from 'react';
import './styles.scss';

/**
 * This component is a button
 *
 * @param {boolean} disabled - Is the button disabled?
 * @param {string} id - The component ID
 * @param {function} onClick - Custom event handler
 * @param {string} styles - Custom classes to customize the component
 * @param {string} text - The text to be inserted into the page
 * @constructor
 */
const Button = ({ disabled, id, onClick, styles, text }) => (
    <button className={styles} disabled={disabled} id={id} onClick={onClick}>
        <span>{text}</span>
    </button>
);

Button.defaultProps = {
    disabled: false,
    onClick: null,
    styles: '',
};

Button.propTypes = {
    disabled: PropTypes.bool,
    id: PropTypes.string.isRequired,
    onClick: PropTypes.func,
    styles: PropTypes.string,
    text: PropTypes.string.isRequired,
};

export default Button;
