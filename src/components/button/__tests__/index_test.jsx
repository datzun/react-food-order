import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

describe('Button Component (Snapshot)', () => {
	it('renders Button component', () => {
		const component = shallow(
			<Module
				id="id"
				onClick={() => {}}
				styles="custom-class"
				text="text"
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('calls onClick when clicked', () => {
		const spy = jest.fn();
		const app = shallow(
			<Module
				id="id"
				onClick={() => spy()}
				styles="custom-class"
				text="text"
			/>
		);
		const input = app.find('button');
		input.simulate('click');
		expect(spy).toHaveBeenCalled()
	});

	it('does not call onClick if not clicked (sanity)', () => {
		const spy = jest.fn();
		shallow(
			<Module
				id="id"
				onClick={() => spy()}
				styles="custom-class"
				text="text"
			/>
		);
		expect(spy).not.toHaveBeenCalled()
	});
});
