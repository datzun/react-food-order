/**
 * @exports Breadcrumbs
 */
import PropTypes from 'prop-types';
import React from 'react';
import './styles.scss';

/**
 * Gets the active class per the location.pathname
 *
 * @param {array} crumb - The given crumbs
 * @returns {string}
 */
const getActiveClass = crumb => (window.location.pathname === crumb.path ? 'active' : '');

/**
 * This is the breadcrumbs component
 *
 * @param {object} props - Passed properties
 * @param {array} props.crumbs - The list of crumbs
 * @constructor
 * @returns {XML} - Breadcrumbs
 */
const Breadcrumbs = props => (
    <ul className="breadcrumbs">
        {props.crumbs.map(crumb => (
            <li className={getActiveClass(crumb)} key={crumb.name}>
                <span>{crumb.name}</span>
            </li>
        ))}
    </ul>
);

Breadcrumbs.propTypes = {
    crumbs: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            path: PropTypes.string,
        })
    ).isRequired,
};

export default Breadcrumbs;
