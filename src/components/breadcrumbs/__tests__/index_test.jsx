import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

describe('Breadcrumbs Component (Snapshot)', () => {
	it('renders Breadcrumbs component', () => {
		const component = shallow(
			<Module
				crumbs={[{
					name: 'first',
					path: '/'
				}, {
					name: 'test',
					path: '/test'
				}]}
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('sets active class', () => {
		const component = shallow(
			<Module
				crumbs={[{
					name: 'first',
					path: '/'
				}, {
					name: 'test',
					path: '/test'
				}]}
			/>
		);
		expect(component.find('.active').length).toBe(1);
	});
});
