import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

let wrapper;

beforeEach(() => {
	wrapper = shallow(<Module />);
});

describe('Done Component (Snapshot)', () => {
	it('renders Done component', () => {
		expect(wrapper).toMatchSnapshot();
	});
});