/**
 * @exports Done
 */

import React from 'react';
import Lottie from 'react-lottie';
import * as animationData from './food.json';
import './styles.scss';

/**
 * The done display
 *
 * @constructor
 * @returns {XML} - Done
 */
class Done extends React.Component {

    constructor(props) {
        super(props);
        this.state = { isStopped: false, isPaused: false };
    }

    render() {
        const defaultOptions = {
            animationData,
            autoplay: true,
            loop: true,
            rendererSettings: {
                preserveAspectRatio: 'xMidYMid slice'
            }
        };

        return <div className="panel done">
            <Lottie
                height={300}
                isPaused={this.state.isPaused}
                isStopped={this.state.isStopped}
                options={defaultOptions}
                width={300}
            />
            <a href="/">Order more?</a>
        </div>;
    }
}

Done.propTypes = {};

export default Done;
