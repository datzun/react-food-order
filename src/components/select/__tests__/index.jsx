import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

describe('Select Component (Snapshot)', () => {
	it('renders Select component', () => {
		const component = shallow(
			<Module
				id="id"
				onChange={() => {}}
				options={[{
					disabled: false,
					label: 'label',
					value: 'value'
				}]}
				styles="custom-class"
				value="4"
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('renders Select component w/ label', () => {
		const component = shallow(
			<Module
				id="id"
				label="label"
				onChange={() => {}}
				options={[{
					disabled: false,
					label: 'label',
					value: 'value'
				}]}
				styles="custom-class"
				value="4"
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('calls onChange when changed', () => {
		const spy = jest.fn();
		const app = shallow(
			<Module
				id="id"
				onChange={() => spy()}
				options={[{
					disabled: false,
					label: 'label',
					value: 'value'
				}]}
				styles="custom-class"
				value="4"
			/>
		);
		const input = app.find('select');
		input.simulate('change');
		expect(spy).toHaveBeenCalled()
	});

	it('does not call onChange if not changed (sanity)', () => {
		const spy = jest.fn();
		shallow(
			<Module
				id="id"
				onChange={() => spy()}
				options={[{
					disabled: false,
					label: 'label',
					value: 'value'
				}]}
				styles="custom-class"
				value="4"
			/>
		);
		expect(spy).not.toHaveBeenCalled()
	});
});
