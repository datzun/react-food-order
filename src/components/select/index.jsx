/**
 * @exports Select
 */
import PropTypes from 'prop-types';
import React from 'react';
import './styles.scss';

/**
 * This is the select component
 *
 * @param {object} props - Passed properties
 * @param {string} props.id - Element ID
 * @param {string} props.label - Label for the given select field
 * @param {function} props.onChange - Handles changing of select value
 * @param {object} props.options - Options
 * @param {string} props.styles - Passed in styling
 * @param {string} props.value - The selected value
 * @constructor
 * @returns {XML} - Select
 */
const Select = props => (
    <React.Fragment>
        {props.label && <label htmlFor={props.id}>{props.label}</label>}
        <select
            className={props.styles}
            id={props.id}
            onChange={props.onChange}
            value={props.value}
        >
            {props.options.map(option => (
                <option key={option.value} value={option.value} disabled={option.disabled}>
                    {option.label}
                </option>
            ))}
        </select>
    </React.Fragment>
);

Select.defaultProps = {
    label: null,
    onChange: () => null,
    options: [],
    styles: '',
    value: '',
};

Select.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func,
    options: PropTypes.arrayOf(
        PropTypes.shape({
            disabled: PropTypes.bool,
            label: PropTypes.string,
            value: PropTypes.string,
        })
    ),
    styles: PropTypes.string,
    value: PropTypes.string,
};

export default Select;
