import React from 'react';
import { shallow } from 'enzyme';
import Module from '../index';

describe('Input Component (Snapshot)', () => {
	it('renders Input component', () => {
		const component = shallow(
			<Module
				id="id"
				max={10}
				onChange={() => {}}
				styles="custom-class"
				type="number"
				value="4"
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('renders Input component w/ label', () => {
		const component = shallow(
			<Module
				id="id"
				label="label"
				max={10}
				onChange={() => {}}
				styles="custom-class"
				type="number"
				value="4"
			/>
		);
		expect(component).toMatchSnapshot();
	});

	it('calls onChange when changed', () => {
		const spy = jest.fn();
		const app = shallow(
			<Module
				id="id"
				max={10}
				onChange={() => spy()}
				styles="custom-class"
				type="number"
				value="4"
			/>
		);
		const input = app.find('input');
		input.simulate('change');
		expect(spy).toHaveBeenCalled()
	});

	it('does not call onChange if not changed (sanity)', () => {
		const spy = jest.fn();
		shallow(
			<Module
				id="id"
				max={10}
				onChange={() => spy()}
				styles="custom-class"
				type="number"
				value="4"
			/>
		);
		expect(spy).not.toHaveBeenCalled()
	});
});
