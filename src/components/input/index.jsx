/**
 * @exports Input
 */
import PropTypes from 'prop-types';
import React from 'react';
import './styles.scss';

/**
 * This is the select component
 *
 * @param {object} props - Passed properties
 * @param {string} props.id - Element ID
 * @param {string} props.label - The label for the given input
 * @param {number} props.max - Max number if type number
 * @param {number} props.min - Min number if type number
 * @param {function} props.onChange - Handles changing of select value
 * @param {string} props.styles - Passed in styling
 * @param {string} props.type - The type of input
 * @param {string} props.value - The selected value
 * @constructor
 * @returns {XML} - Input
 */
const Input = props => (
    <React.Fragment>
        {props.label && <label htmlFor={props.id}>{props.label}</label>}
        <input
            className={props.styles}
            id={props.id}
            max={props.max}
            min={props.min}
            onChange={props.onChange}
            type={props.type}
            value={props.value}
        />
    </React.Fragment>
);

Input.defaultProps = {
    label: '',
    max: null,
    min: null,
    onChange: () => null,
    styles: '',
    type: 'text',
    value: '',
};

Input.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string,
    max: PropTypes.number,
    min: PropTypes.number,
    onChange: PropTypes.func,
    styles: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
};

export default Input;
