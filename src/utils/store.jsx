/**
 * @exports store
 *
 * This file creates a Redux store and adds certain enhancers and middleware
 */

import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import reduxFreeze from 'redux-freeze';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import createHistory from 'history/createBrowserHistory';
import mainReducer from '../components/main/reducer';

export const history = createHistory();

const enhancers = compose(
    applyMiddleware(
        ...[
            thunkMiddleware, // lets us dispatch() functions
            routerMiddleware(history), // handles routing through redux
            reduxFreeze, // prevents state from being mutated anywhere in the app
            createLogger(), // neat middleware that logs actions
        ]
    )
);

const store = createStore(
    combineReducers({
        routing: routerReducer,
        mainReducer,
    }),
    enhancers
);

export default store;
