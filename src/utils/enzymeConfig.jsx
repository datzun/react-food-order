/**
 * @exports enzymeConfig
 *
 * This file is used to polyfill some react features that are available in version 16.
 * @see https://stackoverflow.com/questions/46627353/where-should-the-enzyme-setup-file-be-written
 * @see https://github.com/facebookincubator/create-react-app/issues/3199
 */

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
