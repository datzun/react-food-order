/**
 * @exports setupStore
 *
 * This file creates a Redux store with spies for integration testing purposes
 *
 * @todo: Revisit to look into testing navigating between pages (e.g. MemoryRouter)
 * https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/testing.md
 */

import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import mainReducer from '../components/main/reducer';

const history = createHistory();

export default function setupIntegrationTest() {
    function routingReducer(state = {}, action) {
        return routerReducer(state, action);
    }

    const dispatchSpy = jest.fn(() => ({}));
    const reducerSpy = (state, action) => dispatchSpy(action);

    const emptyStore = applyMiddleware(...[thunk, routerMiddleware(history)])(createStore);
    const combinedReducers = combineReducers({
        reducerSpy,
        routing: routingReducer,
        mainReducer,
    });

    const store = emptyStore(combinedReducers);

    return { store, dispatchSpy };
}
