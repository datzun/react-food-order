module.exports = {
	'extends': [
		'airbnb',           // Baseline rules - https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb
		'prettier',         // Disable rules conflicting with prettier - https://github.com/prettier/eslint-config-prettier
		'prettier/react'    // AirBNB uses react eslint rules too, disable conflicting react rules
	],
	'env': {
		'browser': true,
		'jest': true,
		'node': true
	},
	'plugins': [
		'jsx-a11y',
		'react',
		'import'
	],
	'rules': {
		'camelcase': 'off',
		'comma-dangle': 'off',
		'class-methods-use-this': 'off',
		'indent': ['error', 4, {
			SwitchCase: 1
		}],
		'jsx-a11y/label-has-for': [ 2, {
			"required": {
				"some": [ "nesting", "id" ]
			},
			"allowChildren": false
		}],
		'jsx-a11y/no-static-element-interactions': 'off',
		'jsx-a11y/no-noninteractive-element-interactions': 'off',
		'import/prefer-default-export': 'off',
		'no-param-reassign': ['error', { 'props': false }],
		'no-trailing-spaces': ['error', { 'skipBlankLines': true }],
		'no-use-before-define': 'off',
		'react/jsx-indent': ['error', 4],
		'react/jsx-indent-props': ['error', 4],
		'react/prefer-stateless-function': 'off',
		'valid-jsdoc': ['error', { 'requireParamDescription': false, 'requireReturnDescription': false }],
	},
	'settings': {
		'import/resolver': {
			'webpack': {
				'config': './webpack.config.js'
			}
		}
	}
};
