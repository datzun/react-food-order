const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = {
    devServer: {
        contentBase: './dist'
    },
    entry: ['./src/components/main/index'],
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loaders: [{
	                loader: 'babel-loader',
	                options: {
		                cacheDirectory: true
	                }
                }, 'eslint-loader']
            },
            {
                test: /\.scss$/,
	            exclude: /node_modules/,
                use: [
                	'style-loader',
	                'css-loader?sourceMap',
	                'sass-loader?sourceMap',
	                {
		                loader: 'sass-resources-loader',
		                options: {
			                resources: './src/scss/**/*.scss'
		                }
	                }
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development')
        }),

        new HtmlWebPackPlugin({
            template: './index.html',
            filename: './index.html'
        }),

        new webpack.NamedModulesPlugin(),
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    }
};