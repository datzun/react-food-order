# REACT / REDUX FOOD ORDERING

Pre-ordering food service for hungry people!

=============================================================================================

## Table of contents

*   [Quick start](#quick-start)
*   [Available npm commands](#available-commands)
*   [Comment](#comments)

## Quick start

From the command line:

*   From the root directory, then run `yarn install`

## Available commands

*   `npm start`
    Starts a development server and launches the app.

*   `npm test`
    Runs the tests and exits.

*   `npm run test:watch`
    Runs and watches the tests.

## Comments

No production build at this time.
